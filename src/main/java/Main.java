
import io.grpc.ManagedChannel;
import ir.tourismit.tit.wfms.*;

import java.util.Date;
import java.util.Scanner;

public class Main {

    private static Scanner scanner;
    private static ManagedChannel channel;
    private static ServiceGrpc.ServiceBlockingStub stub;

    public static void main(String[] args){

        scanner = new Scanner(System.in);
        channel = new ServerConnection().getChannel();
        stub = ServiceGrpc.newBlockingStub(channel);

        while (true){
            System.out.print("What do you want to do? (getMessages:1 - getUsers:2 - sendMessage:3)  ");
            String c = scanner.nextLine();
            switch (c){
                case "1" :
                    getMessages();
                    break;
                case "2" :
                    getUsers();
                    break;
                case "3":
                    sendMessage();
                    break;
                default:
                    System.out.println("Invalid input\nEnter number of commands");
            }
            System.out.println("\n\n");
        }
    }

    private static void sendMessage() {
        System.out.print("What's your id: ");
        String fromID = scanner.nextLine();
        System.out.print("What's destination id: ");
        String toID = scanner.nextLine();
        System.out.print("Write your message: ");
        String body = scanner.nextLine();

        MessageResponse response = stub.send(MessageInfo.newBuilder()
                .setFromId(fromID)
                .setBody(body)
                .setToId(toID)
                .setDate(new Date().toString())
                .build());
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println(response.getStatus());
    }

    private static void getUsers() {
        System.out.print("What's your id: ");
        String fromID = scanner.nextLine();

        Users users = stub.openChats(UserInfo.newBuilder()
                .setUserId(fromID)
                .build());
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("Your user list:");
        for (String string: users.getUserList())
            System.out.println(string);
    }

    private static void getMessages() {
        System.out.print("What's your id: ");
        String fromID = scanner.nextLine();
        System.out.print("What's destination id: ");
        String toID = scanner.nextLine();

        Messages messages = stub.getMessages(Chat.newBuilder()
                .setToId(toID)
                .setFromId(fromID)
                .build());
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("Your messages: ");
        for (MessageInfo messageInfo : messages.getMessageList()){
            System.out.println(messageInfo.toString());
        }
    }
}
